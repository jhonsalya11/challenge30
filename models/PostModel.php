<?php 

class PostModel
{
    private $db;
    private $table = 'posts';

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllPost()
    {
        $posts = $this->db->getPost($this->table, 'created_at');
        return $posts;
    }

    public function addNewPost($posts)
    {
        $this->db->add($posts, $this->table);
    }
}