<?php 

class Controller
{
    public function view($view, $posts = [])
    {
        require_once "../views/" . $view . ".php";
    }

    public function model($model)
    {
        require_once "../models/" . $model . ".php";
        return new $model;
    }
}

?>