<?php 
class Post extends Controller
{
    public function index()
    {
        $posts = $this->model('PostModel')->getAllPost();
        // var_dump($posts);
        $this->view('post/index', $posts);
    }

    public function add()
    {
        $validMessage = $this->validation();
        if (empty($validMessage)) {
            $this->model('PostModel')->addNewPost($_POST);
        }
        // redirect browser
        header('Location: ' . BASEURL . '?msg='.$validMessage);
        exit();
    }

    // form validation
    public function validation()
    {
        $msg = '';
        if (empty($_POST["title"])) {
            $msg = $msg . 'Title must be filled in<br>';
        } else if (strlen($_POST["title"]) < 10 || strlen($_POST["title"]) > 32 ) {
            $msg = $msg . 'Your title must be 10 to 32 characters long<br>';
        }

        if (empty($_POST["body"])) {
            $msg = $msg . 'Message must be filled in<br>';
        } else if (strlen($_POST["body"]) < 10 || strlen($_POST["body"]) > 200 ) {
            $msg = $msg . 'Your message must be 10 to 200 characters long<br>';
        }
        
        return $msg;
    }
}

?>